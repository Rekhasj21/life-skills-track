# Prevention of Sexual Harassment

## Questions

**What kinds of behaviour cause sexual harassment?**

- Verbal:
  - Comments about clothing, person's body or gender based jokes.
- Visual:
  - Posters drawing, email or text of a sexual nature
- Physical:
  - Inappropriate touching
  - Quid pro quo- this for that this happens when an employer or supervisor uses job rewards or punishments to coerce an employee into a sexual
  - Hostile work environment- occurs when employee behavior interferes with the work performance of another or makes another employee feel so uncomfortable

**What would you do in case you face or witness any incident or repeated incidents of such behaviour?**

- Follow employers policies and procedures and not engage in workplace bullying and harassment
- Need to report it when we see it or experience it to the higher authority.
- Bottom line is hazard that poses a risk to the health and safety of workers as part of their responsibility to protect worker safety bullying and harassment in a workplace.
