# Energy Management

## Question 1

**What are the activities you do that make you relax - Calm quadrant?**

- Relaxation
- Taking a walk
- Good structure and daily routine - having time for eating, sleeping, working and chilling
- Meditation

## Question 2

**When do you find getting into the Stress quadrant?**

- Tension
- Anger
- Fear
- Not able to complete the task assigned.

## Question 3

**How do you understand if you are in the Excitement quadrant?**

- Surprise
- Happiness
- Enjoyment
- Satisfaction

## Sleep is your superpower

## Question 4

**Paraphrase the Sleep is your Superpower video in detail.**

- Need sleep before and after learning to prepare your brain and save those memories .
- Hippocampus it's like a informational inbox of our brain, good at recieving new memory files and hold them.
- Lack of sleep results in aging and in Alzheimer's disease as well.
- Now, in the spring, when we lose one hour of sleep, we see a subsequent 24-percent increase in heart attacks that following day.In the autumn, when we gain an hour of sleep, we see a 21-percent reduction in heart attacks.
- Natural killer cells like secret service agents.They are very good at identifying dangerous, unwanted elements and eliminating them
- Link between lack of sleep and cancer is now so strong
- The shorter your sleep, the shorter your life.
- Tips for good sleep
  - Regularity
  - Keep it cool (to fall asleep in a room that's too cold than too hot)

## Question 5

**What are some ideas that you can implement to sleep better?**

- Regularity
- Keep it cool (to fall asleep in a room that's too cold than too hot)

## Brain Changing Benefits of Exercise

## Question 6

**Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points.**

- Prefrontal cortex, right behind your forehead,critical for things like decision-making, focus, attention and your personality.
- Hippocampus is a key structure critical for your ability to form and retain new long-term memories for facts and events.
- Exercise:
  - It has immediate effects on your brain.
  - You get better focus and attention.
  - Long-lasting increases in effects of mood.
- The rule of thumb is you want to get three to four times a week exercise(minimum 30 minutes an exercise session).

## Question 7

**What are some steps you can take to exercise more?**

- 30 minutes of exercise
- Add an extra walk, like taking stair case.
- Brigning some exercise into life.
