# Active Listening

## Steps to do in Active Listening 

1. Avoid getting distracted from your own thoughts
2. Focus on the topic 
3. Try to listen to the person who is speaking 
4. Try not to interrupt the other person and 
5. let them finish first and then respond
6. Show sign of listening using body language 
7. Take notes that is important during conversation

## Reflective Listening

1. Listen more and than talk
2. Respond to the questions rather than leading the other
3. Clarifying what the others asked and not asking what we belive
4. Responding with acceptance and not in fake concern
5. It encourages the speaker to talk about more things
6. Its not repeating what was said but a practice of expressing with genuine understanding

## Obstacles in listening

 1. External noises can distract us from listening attentively
 2. If we're upset or stressed, we may not be able to focus on the conversation
 3. If we're not interested in the topic being discussed, we may tune out and miss important details.
 4. If the speaker has a different language background , we may struggle to understand their message.
 
## What can you do to improve your listening?

 1. Try to find a quiet space to listen and remove distractions.
 2. Try to set aside biases and approach the conversation with an open mind.
 3. try to regulate your emotions before entering into a conversation to ensure you can listen attentively.
 4. Be an active listner this involves focusing on the speaker, asking questions, and summarizing what they said to ensure you understood their message.
 5. try to understand the speaker's point of view, even if you don't agree with it, and ask questions to clarify any points you didn't understand.
 
## When do you switch to Passive communication style in your day to day life?

  when our needs are more important than others for example our loved ones
 
## When do you switch into Aggressive communication styles in your day to day life?
    
   Need to be aggressive when you want our things to be done or speak up for our needs
 
## When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

 1. it occur when someone wants to express their anger, but they are not comfortable doing so in a direct and open way but use inderect negative feelings.
 2. when we feel that their needs or wants are being ignored.
 3. if we feel uncomfortable expressing our emotions openly.
 4. when we feel disrespected.
 5. when we want to avoid responsibility.
 
## How can you make your communication assertive? You can analyse the videos and then think what steps you can apply in your own life?

 1. your needs are important so are mine
 2. which means telling our needs and feelings clearly and honestly. while respecting feelings of others
 3. Use clear and specific language when expressing your thoughts, feelings, or needs. 
 4. This can help to avoid misunderstandings and ensure that the other person understands what you are trying to say.
 5. When expressing your thoughts and feelings, it's important to remain calm and avoid getting angry.
 6. Listening to the other person and trying to understand their point of view. This can help in understanding, and make it easier to communicate assertively.
 7. Avoid apologizing unnecessarily.
 8. Use positive language to express your thoughts and feelings, focusing on what you want to achieve, rather than what you want to avoid.