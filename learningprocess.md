# Learning Process

## 1. How to Learn Faster with the Feynman Technique

### Question 1

**What is the Feynman Technique? Paraphrase the video in your own words.**

1. Study Less, Study smart
2. We should be able to teach to someone what we have learnt
3. Feynman is a great scientist and also a great teacher and a great explainer
4. He was able to explain complex concepts into a simple one.
5. Step1 is to write done a concept on a piece of paper
6. Step2 is to explain the concept to someone else and make sure you are able to use this concept in practice.
7. Step3 is to identify the area that you got stcuk and work through those concepts and examples untill you understand
8. Step4 is to look at up to your explanation and try to use technical terms.
9. The thing here is how we are learning the concepts and using it in simple terms.

## 2. Learning How to Learn TED talk by Barbara Oakley

### Question 2

**What are the different ways to implement this technique in your learning process?**

1. Taking notes
2. Learning the concepts deeply
3. Explain it to somebody else to see where we are stcuk
4. Try to use the technical words as possible
5. See ourselfs where we are lagging and revise again and understand the concepts clearly
6. Try to use those concepts practically

### Question 3

**Paraphrase the video in detail in your own words.**
We should be passionate about our job. Try to learn new language.
Language and culture are important but technology is also important as a developer.
Get help from others who knows the concept very well and can teach the best and easiest way to you.
**Focus mode:**
Thinking and focusing on something that you already knew.
**Diffuse Mode:**
Here we need to think in a different way and come up with a new idea
When learning we should go back and forth and try to learn the concepts if get stucked allow diffused mode do their work.
We trying to solve a new problem or want to get new idea but get stuck

- Keep on working untill then automatically it disappers.
- Turn your attention away you feel better just like a **Pomodoro Technique**
- Relaxation is also important part of learning process.
- Dont't compare your learning with others everyone has thier own different experience(**Rabbit trails**).
Steps to learn most effectively:
- Exercise to increase our ability to learn and remember.
- Tests: Test yourself all the time like mini test put all together(**Flash Cards**).
- Recall: Look at a page and look away and see what you can remember.

## Question 4

**What are some of the steps that you can take to improve your learning process?**

- Learn from Diffuse mode and use it in focused mode.
- Pomodoro Technique
- Exercise
- Tests
- Reacll

## 3. Learn Anything in 20 hours

### Question 5

**Your key takeaways from the video? Paraphrase your understanding.**

- Learning new things through trail and error.
- Check how to learn and learn Quikly.
- See how much time you take to learn a new skill
- If you want to be good at it then learn for 10000 hours according to Anders.
- Practice a lot, practice well and you will do extremely well and will reach the top of your feild.
- We can get good at things with just a little bit of practice.
- 20 hours of foused practice to learn something is enough to get good at things.
**Deconstruct the skill**
- Decide exactly what you want to do and then look in to the skill and break it down into smaller pieces which helps you and practice it first
**Learn enough to self correct**
- Get 3 to 5 resources about what you want to learn, self correct or self edit as you practice.
**Remove Barriers to practice**
- Remove distractions
- Willpower
Stick to practice long enough to get rewards. The major barrier's emotional. we are scared.

### Question 6

**What are some of the steps that you can while approaching a new topic?**

- Find key take ways to learn skills quikly
- Practice a lot, practice well and you will do extremely well and will reach the top of your feild.
- Keep foused atleast 20 hours to learn something new.
- Deconstruct the skils.
- Learn enough to self correct.
- Remove barriers to practice.
