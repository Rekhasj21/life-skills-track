# Focus Management

## Question 1

### 1. What is Deep Work

- Focusing without distraction
- Undistracted concentration.
- Coding is a classic example of deep work, the problem that has to be solved has all of the tools and we have to combine them somehow creatively.

## Question 2

### Paraphrase all the ideas in the above videos and this one in detail

- At least an hour of focus, if we are coding for 30 minutes the actual work might be around 15 to 20 minutes
- Deadlines are like motivational signals, the exact time I'm going to work on this and we don't have to debate with ourselves every minute about taking a break.
- Deep work requires a state of distraction‐free concentration to push your cognitive capabilities to their limit and create new value.
- If you want to develop skills and produce work that the world considers rare and valuable, you need to develop a daily deep work ritual.  
- Deep work stratagies
  - Schedule your distraction periods
  - Deep work ritual
  - Evening shutdown- Unfinished tasks and action plan

## Question 3

### How can you implement the principles in your day to day life?

- Trying to focus without distraction at least for a specified period.
- Scheduling our distraction periods
- Following deep work ritual
- Planning the unfinished tasks and action plan

## Question 4

### Dangers of Social Media-Key takeaways from the video

- Author wants to deliver to us that even without social media we don't have to worry but still can have friends and know what's happening around us and be successful professionally.
- It's not a fundamental technology but it borrows some of its.
- Social media tools are designed to be addictive.
- If we produce something rare and valuable the market will value it even without social media.
- So social media use is not harmless, it can have a significant negative impact on your ability to thrive in the economy.
- The more you use social media, the more likely you are to feel lonely or isolated.
- Without social media -it can be quite productive
