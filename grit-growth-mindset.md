# Grit and Growth Mindset

## 1. Grit

### Question 1

**Paraphrase (summarize) the video in a few lines. Use your own words.**

1. Everyone can learn the material if they work hard and long enough
2. As a psychologist she came to know that it wasn't social intelligence, good looks,physical health, and IQ as a predictor of success
3. It was Grit
   - Grit is passion for long terms goals not sticking with your future, day in, day out but for years
4. The best idea to build grit is
   - Growth mindset
5. We have to willing to fail, to be wrong, to start over again with lessons learned.

### Question 2

**What are your key takeaways from the video to take action on?**

1. Need to be gritty
2. Take our best ideas, test them and measure our success
3. We have to willing to fail, to be wrong, to start over again with lessons learned.

## 2. Introduction to Growth Mindset

### Question 3

**Paraphrase (summarize) the video in a few lines in your own words.**

1. Two ways to think about learning
   - Fixed mindset
   - Growth mindset
2. Fixed Mindset
   - They belive some people are just naturally good at things and others are not
   - Skills are bron
   - You can't learn and grow
3. Growth Mindset
   - They believe that skills and intelligence are grown and developed
   - Skills are built
   - You can learn and grow
   - It creates a solid foundation for great learning
4. Mindset have a major influence on people's ability to learn.
5. Characteristics of mindsets
   - Beliefs
   - Focus
6. Key ingredients to growth
   - Efforts: Useful- leads to growth
   - Challenges: Frame as an oppurtunity
   - Mistakes: Use them to learn
   - Feedback: Appreciate and use

### Question 4

**What are your key takeaways from the video to take action on?**

1. Mindset have a major influence on people's ability to learn
2. Growth Mindset is the key to solid foundation for great learning
3. Belive and focus are the characteristics of mindset
4. Put efforts, take up challenges, do mistakes, get feedback and these are the key ingredients to growth.

## 3. Understanding Internal Locus of Control

### Question 5

**What is the Internal Locus of Control? What is the key point in the video?**

1. Belive that you have control over your life and you will never have issues with motivation in your life
2. Start feeling motivated all of the time
3. Solve problems in our own life and take somtime and see it was our action that solve the problem.

## 3. How to build a Growth Mindset

### Question 6

**Paraphrase (summarize) the video in a few lines in your own words.**

1. Belive in your ability to figure out how things works
2. Question your assumptions, think how different you are probably today than you are ten years ago
3. Develop your own life curriculum for developing a long-term growth mindset
4. When you're growing you have a lot of discouragement and at those time honor the struggle
5. Believe in your ability that you can take up a new challenge and can handle it
6. Maintain checklist.

### Question 7

**What are your key takeaways from the video to take action on?**

1. Beliving in my ablitiy whatever may be the situation.
2. Maintain checklist of my short and long term goals.
3. Question my assumptions.

## 4. Mindset - A MountBlue Warrior Reference Manual

### Question 8

**What are one or more points that you want to take action on from the manual?**

1. I will stay with a problem till I complete it. I will not quit the problem.
2. I will understand each concept properly.
3. I will use the weapons of Documentation, Google, Stack Overflow, Github Issues and Internet before asking help from fellow warriors or look at the solution.
4. I will take ownership of the projects assigned to me. Its execution, delivery and functionality is my sole responsibility.
5. I will wear confidence in my body. I will stand tall and sit straight.

By taking these actions, you can develop a mindset that is focused on problem-solving and self-improvement. They can help you to become a better learner, a better problem-solver, and a more confident individual.
