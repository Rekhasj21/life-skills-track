# Good Practices for Software Development

## Question 1

### What is your one major takeaway from each one of the 6 sections. So 6 points in total.

**Gathering requirements:** It is crucial to document and share requirements with the team to ensure everyone is on the same page. Frequent feedback and clarification can help prevent misunderstandings and ensure successful implementation.

**Always over-communicate:** Effective communication is key to successful remote work. Keeping team members informed about changes, issues, and progress can help ensure everyone stays on track. Using group chats and keeping video on during meetings can help improve communication and rapport.

**Stuck? Ask questions:** Asking questions effectively can help you get the answers you need. Clear explanations, examples, and visuals can make it easier for others to understand the problem and provide helpful solutions.

**Get to know your teammates:** Building relationships with team members can improve communication and collaboration. Making time for team meetings and finding out about their schedules can help you better connect with them.

**Be aware and mindful of other team members:** Being considerate of others' schedules and communication preferences can help build stronger relationships and ensure effective collaboration. Consolidating questions into a single message and being available for real-time conversations can help prevent delays.

**Doing things with 100% involvement:** Staying focused and productive during remote work can be challenging. Prioritizing deep work, minimizing distractions, and taking care of physical needs like food and exercise can help improve concentration and productivity.

## Question 2

### Which area do you think you need to improve on? What are your ideas to make progress in that area?

### I need to improve in over-communicate

**Make a communication plan:** Create a plan for how you will communicate with your team, including what channels you will use, how often you will communicate, and what types of information you will share.
**Follow up:** If you've communicated a change or update, follow up to make sure that your team members received the information and understand what is expected of them. This can help avoid confusion or misunderstandings down the road.
**Seek feedback:** Ask your team members for feedback on your communication style and whether there are areas where you could improve. This can help you identify any blind spots and make adjustments as needed.
