# Tiny Habits

## Question 1

### Tiny Habits - Your takeaways from the video (Minimum 5 points)

- Celebrate your victory
- Behaviour change
  - Motivation
  - Ability
  - Trigger the behaviour (triggers are the existing behaviour)
- Format for a 'Tiny Habit"
  - After I [existing habit]
  - I will [new tiny behaviour]
- Look at what you want to change, break it down into tiny behaviour and put them in the right spot and plan accordingly.

## Question 2

### Tiny Habits - Core Message -Your takeaways from the video in as much detail as possible

1. Start with small habits that are easy to do.
2. Focus on doing the habits consistently, rather than trying to do big things all at once.
3. Think of yourself as the type of person who does the habit you want to build.
4. Celebrate your successes, no matter how small they are.
5. Use specific triggers or prompts to help you remember to do the habit.
6. Experiment with different habits and approaches to find what works best for you.

## Question 3

### How can you use B = MAP to make making new habits easier?

1. Choose a new habit you want to start, like doing yoga for a few minutes each day.
2. Think about why you want to do this habit. Maybe you want to feel better or have more energy.
3. Make it easy for yourself to do the habit by starting small, like just doing one minute of yoga each day.
4. Use a reminder, like setting an alarm or leaving a note, to help you remember to do the habit.
5. Celebrate when you do the habit each day, even if it's just for a short time. This will help you build momentum and make it more likely to stick.

## Question 4

### Why it is important to "Shine" or Celebrate after each successful completion of habit?

1. Helps your brain associate the habit with positive feelings, making it more likely you'll want to do it again in the future.
2. Motivates you to keep going and stick with the habit.
3. Builds momentum and creates a sense of accomplishment, which can make it easier to keep doing the habit consistently.

## Question 5

### 1% Better Every Day Video-Your takeaways from the video (Minimum 5 points)

1. Doing small things every day, even for a short amount of time, can lead to big improvements over time.
2. Instead of just focusing on the end goal, enjoy the process of doing small things every day that will lead you towards your goal.
3. Don't worry about being perfect or getting everything right all the time. Just focus on making progress.
4. Keep track of your progress so you can see how far you've come and stay motivated.
5. Remember to be kind to yourself and don't beat yourself up if you slip up or make mistakes. Every day is a new opportunity to improve.

## Question 6

### Write about the book's perspective on habit formation from the lens of Identity, processes and outcomes?

**Identity:** To change your habits, you should focus on the person you want to become and create habits that align with that identity.

**Processes:** Rather than just focusing on the end result, you should focus on creating a system that makes it easy to achieve your desired outcome. By making small changes to your processes, you can create habits that are sustainable and lead to long-term success.

**Outcomes:** While it's important to focus on identity and processes, you should still keep track of your progress towards your desired outcomes. This will help you stay motivated and make adjustments as needed to continue improving.

## Question 7

### Write about the book's perspective on how to make a good habit easier?

"Atomic Habits" by James Clear suggests that to make a good habit easier, you should reduce friction by making it effortless, make it attractive by pairing it with something enjoyable.

Start small by breaking it down into manageable tasks, and focus on the benefits to stay motivated. By following these tips, you can establish and maintain good habits more easily over time.

## Question 8

### Write about the book's perspective on making a bad habit more difficult?

To break a bad habit, "Atomic Habits" suggests making it more difficult by increasing friction, creating a commitment device, and changing your environment.

These strategies make it harder to engage in the bad habit and increase your chances of breaking it.

## Question 9

### Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

1. Make the cue obvious: Keep your coding books and computer in a visible and accessible location, so you see them often.
2. Make it more attractive: Find ways to make coding more enjoyable and rewarding for yourself, such as listening to music or setting up a reward system.
3. Make it easier: Start small and gradually increase the amount of time you spend practicing coding.
4. Make it satisfying: Celebrate your progress and achievements along the way, no matter how small they may seem.

## Question 10

### Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

1. Hide or remove tempting apps from your phone, and turn off notifications to avoid getting distracted.
2. Make your phone less appealing by using a grayscale mode or setting time limits on certain apps.
3. Create barriers to make it harder to access your phone, such as putting it in a different room or using a password or biometric authentication to unlock it.
4. Find alternative activities that are more satisfying than using your phone, such as reading a book or going for a walk, and reward yourself for choosing those activities instead.
